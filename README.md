# Wekan role for Ansible

## Variables

```yaml
wekan:
  domain: board.your.domain
  internal_port: 8001
```

Account dictionary is also expected: https://gitlab.com/n1k0r-ansible/roles/master
